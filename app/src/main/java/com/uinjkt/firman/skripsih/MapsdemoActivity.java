package com.uinjkt.firman.skripsih;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.uinjkt.firman.skripsih.R.id.map;

public class MapsdemoActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private ArrayList<String> ar_name = new ArrayList<String>();
    private ArrayList<Double> ar_lat = new ArrayList<Double>();
    private ArrayList<Double> ar_lng = new ArrayList<Double>();
    private ArrayList<String> ar_cos = new ArrayList<String>();

    private ArrayList<Integer> ar_temporary_meter = new ArrayList<Integer>();
    private ArrayList<JSONArray> ar_temporary_jLegs = new ArrayList<JSONArray>();
    private ArrayList<Integer> ar_temporary_numb_last = new ArrayList<Integer>();
    private ArrayList<Integer> ar_temporary_numb_used = new ArrayList<Integer>();
    private ArrayList<String> ar_temporary_detail = new ArrayList<String>();

    private ArrayList<JSONArray> ar_clean_jLegs = new ArrayList<JSONArray>();

    private String TAG = "Maps1Demo";
    private Marker customMarker;
    private Double arln, arlt;

    private TextView txtDetails;

    private ArrayList<LatLng> points = null;
    private PolylineOptions lineOptions = null;

    private int LastDst = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapsdemo);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(map);
        mapFragment.getMapAsync(this);

        txtDetails = (TextView)findViewById(R.id.txtDetails);

        //https://www.gps-coordinates.net/
        dataMaps();

    }

    private void dataMaps() {

        ar_name.add("SMAN 63");
        ar_lat.add(-6.228576499999999);
        ar_lng.add(106.74885100000006);
        ar_cos.add("S");

        ar_name.add("IPB Bogor");
        ar_lat.add(-6.5581182);
        ar_lng.add(106.72620370000004);
        ar_cos.add("C");

        ar_name.add("Taman Safari");
        ar_lat.add(-6.715771999999999   );
        ar_lng.add(106.94883989999994);
        ar_cos.add("C");

        ar_name.add("UI Depok");
        ar_lat.add(-6.3680739);
        ar_lng.add(106.8295587);
        ar_cos.add("C");

        ar_name.add("UIN Jakarta");
        ar_lat.add(-6.306661);
        ar_lng.add(106.75619600000005);
        ar_cos.add("C");

        ar_name.add("Taman Nasional Gunung Salak");
        ar_lat.add(-6.702510999999999);
        ar_lng.add(106.52269999999999);
        ar_cos.add("C");

        ar_name.add("Ancol");
        ar_lat.add(-6.132975099999999);
        ar_lng.add(106.8266873);
        ar_cos.add("C");

        ar_name.add("Taman Mini");
        ar_lat.add(-6.302445899999999);
        ar_lng.add(106.89515589999996);
        ar_cos.add("C");

        ar_name.add("Summarecon Mall Serpong");
        ar_lat.add(-6.241065799999999);
        ar_lng.add(106.62821050000002);
        ar_cos.add("C");

        ar_name.add("Summarecon Mall Bekasi");
        ar_lat.add(-6.226546099999999);
        ar_lng.add(107.00004790000003);
        ar_cos.add("C");

        ar_name.add("Cibinong City Mall ");
        ar_lat.add(-6.484298799999999);
        ar_lng.add(106.84237899999994);
        ar_cos.add("C");
    }

    private void loadingStart() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.VISIBLE);
    }

    private void loadingStop() {
        ProgressBar loads = (ProgressBar)findViewById(R.id.loadings);
        loads.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        funcOnLoadFirst("");
    }

    public void clickOnLoadFirst(View view){
        ButtonColor("1");
        ar_clean_jLegs.clear();
        txtDetails.setText("");
        ar_temporary_numb_used.clear();
        LastDst=0;
        mMap.clear();
        funcOnLoadFirst("");
    }

    public void funcOnLoadFirst(String kategori) {
        for(int a=0; a<ar_name.size(); a++) {
            arlt = Double.valueOf(ar_lat.get(a));
            arln = Double.valueOf(ar_lng.get(a));
            LatLng latLngValue = new LatLng(arlt, arln);
            View marker;
            if (ar_cos.get(a).equals("S")) {
                marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.icon_for_maps_b, null);
            }else{
                marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.icon_for_maps, null);
            }
            TextView numTxt = (TextView) marker.findViewById(R.id.txtForMaps);
            numTxt.setText(String.valueOf(a));

            customMarker = mMap.addMarker(new MarkerOptions()
                    .position(latLngValue)
                    .title(ar_name.get(a))
                    .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));
        }
        if (!kategori.equals("clear")) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(arlt, arln), 8.0f));
        }
    }

    public void clickGenerateAllPath(View view){
        ButtonColor("2");
        funcGenerateAllPath();
    }

    private void funcGenerateAllPath() {

        /*LatLng origin = new LatLng(ar_lat.get(0), ar_lng.get(0));
        LatLng dest = new LatLng(ar_lat.get(1), ar_lng.get(1));
        String urlnya = getDirectionsUrl(origin, dest);
        getDirectionsByUrl(urlnya);
        LatLng dest2 = new LatLng(ar_lat.get(2), ar_lng.get(2));
        String urlnya2 = getDirectionsUrl(dest, dest2);
        getDirectionsByUrl(urlnya2);*/

        for (int q=1; q<ar_name.size(); q++){

            if (!ar_temporary_numb_used.contains(q)) {
                Log.v("cek_Q",String.valueOf(LastDst));
                LatLng origin = new LatLng(ar_lat.get(LastDst), ar_lng.get(LastDst));
                LatLng dest = new LatLng(ar_lat.get(q), ar_lng.get(q));
                String urlnya = getDirectionsUrl(origin, dest);
                getDirectionsByUrl(urlnya, LastDst, q);
            }

        }

    }

    public void clickClearBadPath(View view){
        ButtonColor("3");
        funcClearBadPath();
    }

    private void funcClearBadPath() {

        if (!ar_temporary_numb_used.contains(0)){
            ar_temporary_numb_used.add(0);
        }
        if (ar_temporary_meter.size()!=0) {
            int terkecil = 0;
            int indexterkecil = 0;

            terkecil = Collections.min(ar_temporary_meter);
            Log.v("cek_terkecil",String.valueOf(terkecil));
            indexterkecil = ar_temporary_meter.indexOf(terkecil);
            Log.v("cek_indexterkecil",String.valueOf(indexterkecil));
            txtDetails.setText(txtDetails.getText().toString()+ar_temporary_detail.get(indexterkecil));
            ar_clean_jLegs.add(ar_temporary_jLegs.get(indexterkecil));
            LastDst = ar_temporary_numb_last.get(indexterkecil);
            if (!ar_temporary_numb_used.contains(LastDst)){
                ar_temporary_numb_used.add(LastDst);
            }
            Log.v("cek_lastdst",String.valueOf(LastDst));

            mMap.clear();
            funcOnLoadFirst("clear");

            for (int a=0; a<ar_clean_jLegs.size(); a++) {
                drawSteps(ar_clean_jLegs.get(a),"");
            }

            ar_temporary_meter.clear();
            ar_temporary_jLegs.clear();
            ar_temporary_numb_last.clear();
            ar_temporary_detail.clear();

        }

    }

    private String getDirectionsUrl(LatLng origin, LatLng dest){
        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
        return url;
    }

    private void getDirectionsByUrl(String urlnya, final int ori, final int dst) {
        //txtDetails.setText("");
        ar_temporary_meter.clear();
        ar_temporary_jLegs.clear();
        ar_temporary_numb_last.clear();
        ar_temporary_detail.clear();

        loadingStart();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, "",
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jObject = new JSONObject(response.toString());
                            String status = jObject.getString("status");
                            if(status.equals("OK")) {

                                /*mulai*/
                                JSONArray jRoutes = null;
                                JSONArray jLegs = null;
                                int distance = 0; //meters
                                int duration = 0; //minutes

                                jRoutes = response.getJSONArray("routes");

                                /** Traversing all routes */
                                for(int i=0;i<1;i++){ //jRoutes.length() //kita pake 1 rute aja asumsi sementara dr uji coba bahwa default free API 1 route
                                    jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");

                                    distance = (Integer) ((JSONObject)((JSONObject)jLegs.get(i)).get("distance")).get("value");
                                    duration = (Integer) ((JSONObject)((JSONObject)jLegs.get(i)).get("duration")).get("value");
                                    //Log.d(TAG, String.valueOf(distance)); Log.d(TAG, String.valueOf(duration));

                                    //Log.d(TAG, "B "+ori+"-"+dst);
                                    ar_temporary_meter.add(distance);
                                    ar_temporary_jLegs.add(jLegs);
                                    ar_temporary_numb_last.add(dst);
                                    ar_temporary_detail.add("Maka jalur tercepat adalah ("+ori+"-"+dst+") :\n"+funcMtoKm(Double.valueOf(distance))+" km & "+funcStoMins(duration)+" mins\n\n");
                                    drawSteps(jLegs,"inet");

                                    txtDetails.setText(txtDetails.getText().toString()+"Jalur ("+ori+"-"+dst+") : "+funcMtoKm(Double.valueOf(distance))+"km & "+funcStoMins(duration)+" mins\n");

                                }
                                /*selesai*/

                                loadingStop();
                            }else {
                                loadingStop();
                                Toast.makeText(getApplicationContext(),"Sorry, data not found !",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {}
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), TAG);
                Log.d(TAG, "FAIL!");
                try {
                    loadingStop();
                    Toast.makeText(getApplicationContext(),"Request error !",Toast.LENGTH_SHORT).show();
                }catch (Exception a){}
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

    }

    private void drawSteps(JSONArray jLegs, String kategori) {
        try {
            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
            JSONArray jSteps = null;
            List path = new ArrayList<HashMap<String, String>>();
            for(int j=0;j<jLegs.length();j++){
                jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");

                /** Traversing all steps */
                for(int k=0;k<jSteps.length();k++){
                    String polyline = "";
                    polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                    List<LatLng> list = decodePoly(polyline);

                    /** Traversing all points */
                    for(int l=0;l<list.size();l++){
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                        hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                        path.add(hm);
                    }
                }
                routes.add(path);
            }
            nowShowDraw(routes, kategori);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void nowShowDraw(List<List<HashMap<String, String>>> result, String kategori) {
        for(int i=0;i<result.size();i++){
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for(int j=0;j<path.size();j++){
                HashMap<String,String> point = path.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            }
            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            //Random rnd = new Random();
            //lineOptions.color(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
            if (kategori.equals("inet")) {
                lineOptions.color(Color.BLUE);
            }else {
                lineOptions.color(Color.RED);
            }
        }
        // Drawing polyline in the Google Map for the i-th route
        mMap.addPolyline(lineOptions).setWidth(5);
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ActionBar.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    private String funcMtoKm(Double val){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(val/1000.0);
    }

    private String funcStoMins(int val){
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(val/60);
    }

    private void ButtonColor(String val){
        Button btn1 = (Button)findViewById(R.id.btn1);
        Button btn2 = (Button)findViewById(R.id.btn2);
        Button btn3 = (Button)findViewById(R.id.btn3);
        if (val.equals("1")){
            btn1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark));
            btn2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
            btn3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
        }else if (val.equals("2")){
            btn1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
            btn2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark));
            btn3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
        }else{
            btn1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
            btn2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
            btn3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark));
        }
    }

    public static Spanned format_html(String teks){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(teks, Html.FROM_HTML_MODE_COMPACT);
        }else {
            return Html.fromHtml(teks);
        }
    }

}
